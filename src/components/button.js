import React from 'react'

const Button = props => (
    <button className={`button button--${props.decoration}`}>
        {props.buttonIcon && <img className="button__icon" src={props.buttonIcon} alt="" />}
        {props.buttonText}
    </button>
)

Button.defaultProps = {
    decoration: 'underline'
}

export default Button
