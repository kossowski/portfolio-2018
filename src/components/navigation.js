import React, { Component } from 'react'
import { Link } from 'gatsby'

import Gitlab from '../images/brand-gitlab.svg'
import Behance from '../images/brand-behance.svg'
import Xing from '../images/brand-xing.svg'

class Navigation extends Component {
    constructor(props) {
        super(props);

        this.state = { navVisible: false };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(prevState => ({
            navVisible: !prevState.navVisible
        }));
    }

    render() {
        return (
            <nav className={`navigation ${this.props.backButton && 'navigation--flex'}`}>
                {this.props.backButton && <Link to="/"><button className="navigation__back-button" /></Link>}
                <button onClick={this.handleClick} className="navigation__burger">
                    <span className={`burger burger__top ${this.state.navVisible ? "burger__top--close" : ""}`}></span>
                    <span className={`burger burger__bottom ${this.state.navVisible ? "burger__bottom--close" : ""}`}></span>
                </button>

                <div className={`navigation__content ${this.state.navVisible ? "navigation__content--open" : "navigation__content--close"}`}>
                    <div className="navigation__item navigation__item--flex">
                        <Link className="navigation__link underline underline--white" to="/">Startseite</Link>
                        <Link className="navigation__link underline underline--white" to="/about-me/">Über mich</Link>
                        <a className="navigation__link underline underline--white" href="mailto:dennis.kossowski@outlook.com">Kontakt</a>
                    </div>
                    <div className="navigation__item">
                        <div className="navigation__icons-wrapper">
                            <a href="#0" className=""><img className="navigation__icon" src={Gitlab} alt=""></img></a>
                            <a href="#0" className=""><img className="navigation__icon" src={Behance} alt=""></img></a>
                            <a href="https://www.xing.com/profile/Dennis_Kossowski2" target="_blanc" className=""><img className="navigation__icon" src={Xing} alt=""></img></a>
                        </div>
                        {/* <a href="mailto:dennis.kossowski@outlook.com" className="navigation__email">desk@denniskossowski.de</a> */}
                    </div>
                </div>
                <div onClick={this.handleClick} className="navigation__shadow-overlay" />
            </nav >
        )
    }
}

export default Navigation
