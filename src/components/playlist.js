import React from 'react'
import { Link } from 'gatsby'

import { Container, TextElement, Button } from './index'

const Playlist = () => (
    <div className="playlist">
        <Container width="large">
            <div className="grid-wrapper mt-120">
                <iframe className="iframe" title="playlist" src="https://open.spotify.com/embed/user/laybeckz/playlist/4Ye8sgFNYF8fMQv0uppnB2" width="100%" height="100%" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>

                <TextElement align="left" title="Meine Playlist." copy={playlistCopy}>
                    <p className="playlist__copy">
                        Ein paar Sätze über mich gibt es auf der nächsten Seite:
                    </p>
                    <Link to="/about-me"><Button buttonText="Über mich" decoration="border" /></Link>
                </TextElement>
            </div>
        </Container>
    </div>
)

const playlistCopy = "Meine aktuellste Playlist, die ich privat und beim Arbeiten höre."

export default Playlist
