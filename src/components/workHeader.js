import React from 'react'

import { Container } from './index'

class WorkHeader extends React.Component {
    componentDidMount() {
        const AOS = require('aos');
        this.aos = AOS
        this.aos.init()
    }

    componentDidUpdate() {
        this.aos.refresh()
    }

    render() {
        return (
            <div className="work-header">
                <Container width="large">
                    <div className="work-header__wrapper">
                        <h1 className="work-header__headline" data-aos="fade-up">{this.props.headline}</h1>
                        <WorkHeaderItem label={this.props.label} subline={this.props.subline} />
                    </div>
                </Container>
            </div>
        );
    }
}

const WorkHeaderItem = (props) => (
    <div className="work-header__item">
        <span className="work-header__label" data-aos="fade-up">{props.label}</span>
        <p className="work-header__subline" data-aos="fade-up" data-aos-delay="200">{props.subline}</p>
    </div>
)

WorkHeader.defaultProps = {
    headline: "Title"
}

WorkHeaderItem.defaultProps = {
    label: "Label",
    subline: "Subline"
}

export default WorkHeader
