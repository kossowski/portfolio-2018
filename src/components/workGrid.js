import React from 'react'

import { Container, TextElement } from './index'

const WorkGrid = (props) => (
    <div className="work mt-120">
        <Container>
            <TextElement title="Angefangen mit einigen meiner Arbeiten." />
        </Container>
        <Container width="large">
            <div className="work__grid">
                {props.children}
            </div>
        </Container>
    </div>
)

export default WorkGrid
