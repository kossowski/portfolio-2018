import React from 'react'

const ExternalLink = props => (
    <a href={props.link} target="_blanc" className="external-link">{props.name}</a>
)

export default ExternalLink
