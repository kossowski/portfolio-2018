import React from 'react'

import Container from './container'
import TextElement from './textElement'
import ExternalLink from './externalLink'

import SketchLogo from '../images/logos/Sketch_logo_frame.svg'
import AdobeLogo from '../images/logos/adobe.svg'
import HTMLLogo from '../images/logos/HTML5_logo_and_wordmark.svg'
import SassLogo from '../images/logos/Sass_Logo_Color.svg'
import ReactLogo from '../images/logos/React-icon.svg'
import IconPen from '../images/icon-pen.svg'

class SkillsElement extends React.Component {
  componentDidMount() {
    const AOS = require('aos');
    this.aos = AOS
    this.aos.init()
  }

  componentDidUpdate() {
    this.aos.refresh()
  }

  render() {
    return (
      <div className="skills-element mt-120">
        <Container width="large">
          <div className="skills-element__wrapper">
            <TextElement align="left" title={skillsHeadline} copy={skillsCopy}>
            </TextElement>
            <div className="skills-element__grid">
              <Item logo={SketchLogo} delay="100" label="Sketch" />
              <Item logo={AdobeLogo} delay="150" label="Adobe CC" />
              <Item logo={IconPen} delay="200" label="Pen &amp; Paper" />
              <Item logo={HTMLLogo} delay="250" label="HTML" />
              <Item logo={SassLogo} delay="300" label="Sass" />
              <Item logo={ReactLogo} delay="350" label="ReactJS" />

            </div>
            <p className="skills-element__text">
              Abgesehen davon kenne ich mich auch in einigen Hilfprogrammen aus wie <ExternalLink link="https://www.invisionapp.com/" name="inVision" />, <ExternalLink link="https://www.getflow.com/" name="flow" />, <ExternalLink link="https://about.gitlab.com/" name="gitlab" />, <ExternalLink link="https://www.gitkraken.com/" name="gitkraken" />, <ExternalLink link="https://www.netlify.com/" name="netlify" /> und einem Haufen Sketch-Plugins.
            </p>
          </div>
        </Container>
      </div>
    );
  }
}

const Item = props => (
  <div data-aos="fade-up" data-aos-delay={props.delay} className="skills-element__item">
    <img
      className="skills-element__icon"
      src={props.logo}
      alt=""
      type="image/svg+xml"
    />
    <span className="skills-element__label">{props.label}</span>
  </div>
)

const skillsHeadline = "Für Nerds: Womit ich am liebsten arbeite."
const skillsCopy = "Moderne, high-performace Web-Applications benötigen die richtigen Tools in der Entwicklung. Das ist eine Liste meiner aktuellsten Auswahl, mit denen ich am liebsten arbeite."

export default SkillsElement
