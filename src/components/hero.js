import React from 'react'

import { Container, Button } from './index'

import EmailIcon from '../images/email.svg'

const Hero = () => (
  <div className="hero">
    <Container width="large">
      <div className="hero__top">
        <div className="hero__text">
          <h1 className="hero__headline">
            Moin. <span className="hero__emoji" role="img" aria-label="emoji">👋</span>
            <br />
            Ich bin Dennis.
          </h1>
          <p className="hero__intro">Mein Name ist Dennis Kossowski. Ich bin 25 Jahre alt und arbeite als Kommunikationsdesigner und Frontend-Developer. Dies ist mein digitales Portfolio, das vor allem erklären soll was ich beruflich so mache.</p>
          <a href="mailto: desk@denniskossowski.de"><Button buttonIcon={EmailIcon} buttonText="desk@denniskossowski.de" /></a>
        </div>
        <div className="hero__image" />
      </div>
    </Container>
    {/* <div className="hero__bottom" /> */}
  </div>
)

export default Hero
