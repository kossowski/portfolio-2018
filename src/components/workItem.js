import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

const WorkItem = (props) => (
    <Link to={props.workLink} className="work__link">
        <div className="work__item">
            <h3 className="work__headline">{props.headline}</h3>
            <div className="work__wrapper">
                <Img className="work__thumbnail" fluid={props.thumbnail} alt="" title="" />
            </div>
        </div>
    </Link>
)

WorkItem.defaultProps = {
    headline: "Headline"
}

export default WorkItem