import React from 'react'

const Container = (props) => (
    <div className={`container container--${props.width}`}>
        {props.children}
    </div>
)

Container.defaultProps = {
    width: "small"
}

export default Container