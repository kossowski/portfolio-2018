import React from 'react'

import { Container, Button } from './index'

import GitlabLogo from '../images/brand-gitlab.svg'
// import BehanceLogo from '../images/brand-behance.svg'
import EmailIcon from '../images/icons/envelope.svg'
import XingLogo from '../images/icons/brand-xing.svg'
import FooterIcon from '../images/icon-keyboard.svg'

const Footer = props => (
  <div className="footer mt-120">
    <Container>
      <div className="footer__wrapper">
        <button className="button--to-top" onClick={ScrollToTop} />
        {!props.small && (
          <div className="footer__top-layer">
            <img className="footer__icon" src={FooterIcon} alt="" />
            <h3 class="footer__headline">
              Du bist an einer Zusammenarbeit interessiert?
            </h3>
            <p className="footer__tagline">
              Schreibe mir doch einfach auf{' '}
              <a
                className="footer__link"
                href="https://www.xing.com/profile/Dennis_Kossowski2"
                target="_blanc"
              >
                xing
              </a>{' '}
              oder per{' '}
              <a className="footer__link" href="mailto:desk@denniskossowski.de">
                mail
              </a>
              .
            </p>
          </div>
        )}
        <div className="footer__bot-layer">
          <a href="mailto:desk@denniskossowski.de">
            <Button buttonIcon={EmailIcon} buttonText="Email" />
          </a>
          <a
            href="https://www.xing.com/profile/Dennis_Kossowski2"
            target="_blanc"
          >
            <Button buttonIcon={XingLogo} buttonText="Xing" />
          </a>
          <a href="#0">
            <Button buttonIcon={GitlabLogo} buttonText="gitlab" />
          </a>
        </div>
        {/* <Button buttonIcon={BehanceLogo} buttonText="behance" /> */}
        <p className="footer__subline">
          Mein Portfolio ist außerdem Open Source und komplett auf gitlab.com zu
          finden.
        </p>
        <p className="footer__subline">
          <span>{new Date().getFullYear()} · </span>
          <span>MIT · </span>
          <span>Made with ❤︎ in Hamburg</span>
        </p>
      </div>
    </Container>
  </div>
)

function ScrollToTop() {
  var timerHandle = setInterval(function() {
    if (
      document.body.scrollTop !== 0 ||
      document.documentElement.scrollTop !== 0
    ) {
      window.scrollBy(0, -80)
    } else clearInterval(timerHandle)
  }, 8)
}

export default Footer
