import React from 'react'

import TeamIcon from './../images/icons/user-circle.svg'
import ToolsIcon from './../images/icons/paint-roller.svg'
import LinkIcon from './../images/icons/link.svg'
import ExternalIcon from './../images/icons/icon-external.svg'

const WorkDescription = props => (
    <div className="work-description work-description__grid">
        <div className="work-description__grid-item">
            <h3>{props.description}</h3>
            <p>{props.copy}</p>
        </div>
        <div className="work-description__grid-item">
            <h3>{props.information}</h3>
            <DescriptionItem label="Team" specs={props.team} icon={TeamIcon} />
            <DescriptionItem label="Tools" specs={props.tools} icon={ToolsIcon} />
            <DescriptionItem label="Client" specs={props.client} icon={LinkIcon} />
            {props.demo && <DescriptionItem label="Website" link={props.demo} specs={props.link} icon={ExternalIcon} />}
        </div>
    </div>
)

const DescriptionItem = props => (
    <li>
        <img
            className="work-description__icon"
            alt=""
            src={props.icon}
        />
        <span className="work-description__label">{props.label}: </span>
        {props.link ? <a className="text-link" href={props.link} target="_blanc">{props.specs}</a> : <span>{props.specs}</span>}
    </li>
)

WorkDescription.defaultProps = {
    description: 'Projekt Beschreibung',
    copy: 'Lorem Ipsum',
    information: 'Projekt Informationen',
}

DescriptionItem.defaultProps = {
    label: 'Label',
    specs: 'Spec',
}

export default WorkDescription
