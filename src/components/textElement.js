import React from 'react'

const TextElement = (props) => (
    <div className={`text-element text-element--${props.color}`}>
        <div className={`text-element--${props.align}`}>
            <h2 className="text-title">{props.title}</h2>
            {props.copy && <p className="text-copy">{props.copy}</p>}
        </div>
        {props.children}
    </div>
)

TextElement.defaultProps = {
    color: "white",
    align: "center"
}

export default TextElement
