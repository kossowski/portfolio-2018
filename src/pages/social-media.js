import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'

import { Navigation, WorkHeader, WorkDescription, Footer, Container, Layout } from '../components'

const SocialMedia = ({ data }) => (
    <Layout>
        <div className="social-media">
            <Navigation backButton />
            <WorkHeader
                headline="Social Media labello"
                label="Datum"
                subline="2017"
            />
            <Container width="large">
                <div className="social-media__grid">
                    <Img alt="" title="" fluid={data.social_001.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_002.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_003.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_004.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_005.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_006.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_007.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_008.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_009.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_010.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_011.childImageSharp.fluid} />
                    <Img alt="" title="" fluid={data.social_012.childImageSharp.fluid} />
                </div>
                <WorkDescription
                    copy={WorkDescriptionCopy}
                    team="brand:marke"
                    tools="Photoshop CC · Illustrator CC"
                    client="labello"
                    demo="https://www.instagram.com/labello/?hl=de"
                    link="instagram.com/labello"
                />
            </Container>
            <Footer small />
        </div>
    </Layout>
)

const WorkDescriptionCopy = "Über zwei Quartale habe ich den internationalen Labello-Instagram Account mit frischen Posts bestückt."

export const post = graphql`
            fragment post on File {
                childImageSharp {
                    fluid(maxWidth: 600, maxHeight: 600) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
`

export const query = graphql`
            query {
                social_001: file(relativePath:{ eq:"social-media/social_001.jpg"}) {
                    ...post
                },
                social_002: file(relativePath:{ eq:"social-media/social_002.jpg"}) {
                    ...post
                },
                social_003: file(relativePath:{ eq:"social-media/social_003.jpg"}) {
                    ...post
                },
                social_004: file(relativePath:{ eq:"social-media/social_004.jpg"}) {
                    ...post
                },
                social_005: file(relativePath:{ eq:"social-media/social_005.jpg"}) {
                    ...post
                },
                social_006: file(relativePath:{ eq:"social-media/social_006.jpg"}) {
                    ...post
                },
                social_007: file(relativePath:{ eq:"social-media/social_007.jpg"}) {
                    ...post
                },
                social_008: file(relativePath:{ eq:"social-media/social_008.jpg"}) {
                    ...post
                },
                social_009: file(relativePath:{ eq:"social-media/social_009.jpg"}) {
                    ...post
                },
                social_010: file(relativePath:{ eq:"social-media/social_010.jpg"}) {
                    ...post
                },
                social_011: file(relativePath:{ eq:"social-media/social_011.jpg"}) {
                    ...post
                },
                social_012: file(relativePath:{ eq:"social-media/social_012.jpg"}) {
                    ...post
                },
            }
`

export default SocialMedia
