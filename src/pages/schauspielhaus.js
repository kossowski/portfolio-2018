import React from 'react'
import { graphql } from 'gatsby'
import Swiper from 'react-id-swiper/lib/custom'
import Img from 'gatsby-image'

import {
  Navigation,
  WorkHeader,
  WorkDescription,
  Footer,
  Container,
  Layout,
} from '../components'

const Schauspielhaus = ({ data }) => (
  <Layout>
    <div className="schauspielhaus">
      <Navigation backButton />
      <WorkHeader
        headline="Deutsches Schauspielhaus"
        label="Datum"
        subline="2015 – 2016"
      />
      <Container width="large">
        <div className="fullwidth-fix">
          <Swiper {...params}>
            <Img alt="" title="" fluid={data.ds_001.childImageSharp.fluid} />
            <Img alt="" title="" fluid={data.ds_002.childImageSharp.fluid} />
            <Img alt="" title="" fluid={data.ds_003.childImageSharp.fluid} />
            <Img alt="" title="" fluid={data.ds_004.childImageSharp.fluid} />
            <Img alt="" title="" fluid={data.ds_005.childImageSharp.fluid} />
            <Img alt="" title="" fluid={data.ds_006.childImageSharp.fluid} />
            <Img alt="" title="" fluid={data.ds_007.childImageSharp.fluid} />
          </Swiper>
        </div>
        <WorkDescription
          copy={descriptionCopy}
          team="Kunstschule Wandsbek · Hanna LeGrand"
          tools="Photoshop CC · Canon EOS600D"
          client="Deutsches Schauspielhaus · Thalia Theater"
        />
      </Container>
      <Footer small />
    </div>
  </Layout>
)

const descriptionCopy =
  'Diese Fotoreihe ist im Rahmen des Deutschen Schauspielhaus und des Thalia Theaters entstanden. Es zeigt die Erföffnung der Weltpremieren “Weltklimakonferrenz” und Backstage-Szenen aus “Erinnerungen”. Beide Projekte wurden über drei Tage begleitet und dokumentiert. Die Fotoreihe “Erinnerungen” wurde vor der Eröffnung des Stücks, in der Galerie im Deutschen Schauspielhaus, ausgestellt.'

const params = {
  loop: true,
  autoplay: {
    delay: 3000,
    disableOnInteraction: false,
  },
  scrollbar: {
    el: '.swiper-scrollbar',
    hide: false,
  },
}

export const foto = graphql`
  fragment foto on File {
    childImageSharp {
      fluid(maxWidth: 1440, maxHeight: 810) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`

export const query = graphql`
  query {
    ds_001: file(relativePath: { eq: "fotografie/ds_001.jpg" }) {
      ...foto
    }
    ds_002: file(relativePath: { eq: "fotografie/ds_002.jpg" }) {
      ...foto
    }
    ds_003: file(relativePath: { eq: "fotografie/ds_003.jpg" }) {
      ...foto
    }
    ds_004: file(relativePath: { eq: "fotografie/ds_004.jpg" }) {
      ...foto
    }
    ds_005: file(relativePath: { eq: "fotografie/ds_005.jpg" }) {
      ...foto
    }
    ds_006: file(relativePath: { eq: "fotografie/ds_006.jpg" }) {
      ...foto
    }
    ds_007: file(relativePath: { eq: "fotografie/ds_007.jpg" }) {
      ...foto
    }
  }
`

export default Schauspielhaus
