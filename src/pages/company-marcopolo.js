import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'

import {
  Navigation,
  WorkHeader,
  WorkDescription,
  Footer,
  Container,
  Layout,
} from '../components'

const CompanyMarcoPolo = ({ data }) => (
  <Layout>
    <div className="company-mop">
      <Navigation backButton />
      <WorkHeader headline="Company Marc O'Polo" label="Datum" subline="2018" />
      <Container width="large">
        <Img className="fullwidth-fix" alt="" title="" fluid={data.marcopolo_001.childImageSharp.fluid} />
        <WorkDescription
          copy={WorkDescriptionCopy}
          team="slim Interactive"
          tools="Typo3, HTML, SASS"
          client="Marc O'Polo"
          demo="https://company.marc-o-polo.com"
          link="company.marc-o-polo.com"
        />
      </Container>
      <Footer small />
    </div>
  </Layout>
)

const WorkDescriptionCopy =
  'Das Projekt für Marc OPolo Company war mein erstes großes Projekt in Typo3. Angefangen mit Design-Anpassungen im Layout bestand mein Part im wesentlichen darin das Layout im CSS umzusetzen, Microinteractions einzubauen und die DOM-Strukturen zu erweitern.'

export const query = graphql`
  query {
    marcopolo_001: file(relativePath: { eq: "marcopolo/marcopolo_001.png" }) {
      ...ratio32
    }
  }
`

export default CompanyMarcoPolo
