import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'

import {
  Navigation,
  WorkHeader,
  WorkDescription,
  Footer,
  Container,
  Layout,
} from '../components'

const Bundeswehrkarriere = ({ data }) => (
  <Layout>
    <div className="bwkarriere">
      <Navigation backButton />
      <WorkHeader
        headline="Bundeswehr Karriere"
        label="Datum"
        subline="2018"
      />
      <Container width="large">
        <Img
          className="fullwidth-fix"
          alt=""
          title=""
          fluid={data.bundeswehr_001.childImageSharp.fluid}
        />
        <WorkDescription
          copy={WorkDescriptionCopy}
          team="slim Interactive"
          tools="VueJS, SASS"
          client="Bundeswehr"
          demo="https://www.bundeswehrkarriere.de/"
          link="www.bundeswehrkarriere.de/"
        />
      </Container>
      <Footer small />
    </div>
  </Layout>
)

const WorkDescriptionCopy =
  'In dem neuen Webauftritt der Bundeswehr Karriere Seite habe ich im Bereich Fontend-Entwicklung mitgearbeitet. Meine Aufgabe war es das fertige Layout im CSS umzusetzen und ggf. kleinere DOM-Änderungen vorzunehmen. Das besondere war dabei der erstmalige Einsatz des JavaScript Frameworks vue.js'

export const ratio32 = graphql`
  fragment ratio32 on File {
    childImageSharp {
      fluid(maxWidth: 1440, maxHeight: 960) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`

export const query = graphql`
  query {
    bundeswehr_001: file(
      relativePath: { eq: "bundeswehr/bundeswehr_001.png" }
    ) {
      ...ratio32
    }
  }
`

export default Bundeswehrkarriere
