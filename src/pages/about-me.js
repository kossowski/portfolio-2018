import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import Swiper from 'react-id-swiper/lib/custom'

import { Layout, Navigation, Container, Footer } from '../components'

import Client001 from '../images/lazyload/about-me/client_001.svg'
import Client002 from '../images/lazyload/about-me/client_002.svg'
import Client003 from '../images/lazyload/about-me/client_003.svg'
import Client004 from '../images/lazyload/about-me/client_004.svg'
import Client005 from '../images/lazyload/about-me/client_005.svg'
import Client006 from '../images/lazyload/about-me/client_006.png'
import Client007 from '../images/lazyload/about-me/client_007.svg'
import Client008 from '../images/lazyload/about-me/client_008.svg'
import Client009 from '../images/lazyload/about-me/client_009.svg'

const AboutMe = ({ data }) => (
    <Layout>
        <Navigation backButton />
        <div className="about-me">
            <Container width="large">
                <div className="about-me__grid">
                    <div className="about-me__image">
                        <Img className="about-me__img" title="" alt="" fluid={data.profil.childImageSharp.fluid} />
                    </div>
                    <div className="about-me__copy">
                        <h1 className="about-me__headline">I build web-apps.</h1>
                        <p>Die meiste Zeit verbinge ich damit, über neue Web-Apps nachzudenken, sie zu entwickeln und mich dabei weiterzubilden. Zuletzt habe ich für slim interactive in Hannover gearbeitet – eine riesen Erfahrung, bei der ich mich in den Bereichen Design und Coding komplett einbringen konnte.</p>
                        <p></p>
                    </div>
                </div>

                <div className="about-me__interest">
                    <h3>Kunden für die ich arbeiten konnte:</h3>
                    {/* <div className="about-me__interest-grid"> */}
                    <div className="fullwidth-fix">
                        <Swiper {...params}>
                            <img src={Client001} alt="" />
                            <img src={Client002} alt="" />
                            <img src={Client003} alt="" />
                            <img src={Client004} alt="" />
                            <img src={Client005} alt="" />
                            <img src={Client006} alt="" />
                            <img src={Client007} alt="" />
                            <img src={Client008} alt="" />
                            <img src={Client009} alt="" />
                        </Swiper>
                    </div>
                </div>
            </Container>
        </div>
        <Footer />
    </Layout>
)

const params = {
    slidesPerView: 4,
    spaceBetween: 32,
    loop: true,
    autoplay: {
        delay: 3000,
        disableOnInteraction: false
    },
    scrollbar: {
        el: '.swiper-scrollbar',
        hide: false,
    },
    breakpoints: {
        768: {
            slidesPerView: 2,
            spaceBetween: 8,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 16,
        }
    },

}

export const logo = graphql`
    fragment logo on File {
        childImageSharp {
            fluid(maxWidth: 1000, maxHeight: 563) {
                ...GatsbyImageSharpFluid
            }
        }
    }
`

export const query = graphql`
    query ProfilBild {
        profil: file(relativePath:{ eq:"about-me/profilbild.jpg" }) {
            childImageSharp {
                fluid(maxWidth: 600, maxHeight: 600) {
                ...GatsbyImageSharpFluid
                }
            }
        },
        client1: file(relativePath:{ eq:"about-me/client-BDF.jpg" }) {
                ...logo
        }
        client2: file(relativePath:{ eq:"about-me/client-BW.png" }) {
            ...logo
        } 
    }
`

export default AboutMe
