import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'

import {
    Navigation,
    WorkHeader,
    WorkDescription,
    Footer,
    Container,
    Layout,
} from '../components'

const Syndicate = ({ data }) => (
    <Layout>
        <div className="syndicate">
            <Navigation backButton />
            <WorkHeader
                headline="Syndicate Pizza Configurator"
                label="Datum"
                subline="2017"
            />
            <Container width="large">
                <div className="fullwidth-fix">
                    <Img fluid={data.syndicate_001.childImageSharp.fluid} />
                </div>
                <WorkDescription
                    team="Kunstschule Wandsbek"
                    tools="Photoshop CC · inDesign CC · Illustrator CC"
                    client="Kunstschule Wandsbek"
                    copy={syndicateDescription}
                />
            </Container>
            <Footer small />
        </div>
    </Layout>
)

const syndicateDescription = 'Das fiktive Projekt "Syndicate Pizza" bestande aus der Aufgabe eine Website mit einem Configurator für Lebensmittel zu gestalten.'

export const query = graphql`
  query {
    syndicate_001: file(relativePath: { eq: "syndicate/syndicate_001.png" }) {
        childImageSharp {
            fluid(maxWidth: 1400, maxHeight: 9300) {
              ...GatsbyImageSharpFluid
            }
          }
      }
  }
`

export default Syndicate
