import React from 'react'
import { graphql } from 'gatsby'

import {
  Layout,
  Navigation,
  Hero,
  SkillsElement,
  Footer,
  WorkGrid,
  WorkItem,
  Playlist,
} from '../components'

const IndexPage = ({ data }) => (
  <Layout>
    <Navigation />
    <Hero />
    <div className="main">
      <WorkGrid>
        <WorkItem
          headline="Logocollection"
          thumbnail={data.thumbnail_001.childImageSharp.fluid}
          workLink="/logo-collection/"
        />
        <WorkItem
          headline="Deutsches Schauspielhaus"
          thumbnail={data.thumbnail_002.childImageSharp.fluid}
          workLink="/schauspielhaus/"
        />
        <WorkItem
          headline="Bundeswehr Karriere"
          thumbnail={data.thumbnail_003.childImageSharp.fluid}
          workLink="/bundeswehrkarriere/"
        />
        <WorkItem
          headline="Editorial Design"
          thumbnail={data.thumbnail_006.childImageSharp.fluid}
          workLink="/fanzine/"
        />
        <WorkItem
          headline="Social Media"
          thumbnail={data.thumbnail_005.childImageSharp.fluid}
          workLink="/social-media/"
        />
        <WorkItem
          headline="Company Marc O'Polo"
          thumbnail={data.thumbnail_004.childImageSharp.fluid}
          workLink="/company-marcopolo/"
        />
        <WorkItem
          headline="Syndicate Pizza"
          thumbnail={data.thumbnail_007.childImageSharp.fluid}
          workLink="/syndicate/"
        />
      </WorkGrid>
      <SkillsElement />
      <Playlist />
    </div>
    <Footer />
  </Layout>
)

export const thumbnail = graphql`
  fragment thumbnail on File {
    childImageSharp {
      fluid(maxWidth: 1200, maxHeight: 800) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`

export const query = graphql`
  query {
    thumbnail_001: file(relativePath: { eq: "thumbnails/logocollection.jpg" }) {
      ...thumbnail
    }
    thumbnail_002: file(relativePath: { eq: "thumbnails/schauspielhaus.jpg" }) {
      ...thumbnail
    }
    thumbnail_003: file(relativePath: { eq: "thumbnails/bundeswehr.jpg" }) {
      ...thumbnail
    }
    thumbnail_004: file(relativePath: { eq: "thumbnails/marcopolo.jpg" }) {
      ...thumbnail
    }
    thumbnail_005: file(relativePath: { eq: "thumbnails/socialmedia.jpg" }) {
      ...thumbnail
    }
    thumbnail_006: file(relativePath: { eq: "thumbnails/editorial.jpg" }) {
      ...thumbnail
    }
    thumbnail_007: file(relativePath: { eq: "thumbnails/syndicate.png" }) {
      ...thumbnail
    }
  }
`

export default IndexPage
