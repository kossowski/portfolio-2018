import React from 'react'
import { graphql } from 'gatsby'
import Swiper from 'react-id-swiper/lib/custom';
import Img from 'gatsby-image'

import { Navigation, WorkHeader, WorkDescription, Footer, Container, Layout } from '../components'

const Fanzine = ({ data }) => (
    <Layout>
        <div className="fanzine">
            <Navigation backButton />
            <WorkHeader
                headline="Directors Club"
                label="Datum"
                subline="2015"
            />
            <Container width="large">
                <div className="fullwidth-fix">
                    <Swiper {...params} >
                        <Img alt="" title="" fluid={data.editorial_001.childImageSharp.fluid} />
                        <Img alt="" title="" fluid={data.editorial_002.childImageSharp.fluid} />
                        <Img alt="" title="" fluid={data.editorial_003.childImageSharp.fluid} />
                        <Img alt="" title="" fluid={data.editorial_004.childImageSharp.fluid} />
                        <Img alt="" title="" fluid={data.editorial_005.childImageSharp.fluid} />
                    </Swiper>
                </div>
                <WorkDescription
                    copy={descriptionCopy}

                    team="Kunstschule Wandsbek"
                    tools="Adobe inDesign CC · Photoshop CC"
                    client="Kunstschule Wandsbek"
                />
            </Container>
            <Footer small />
        </div>
    </Layout>
)

const descriptionCopy = "Das Fanzine Director’s Club ist ein Editorial-Projekt für Fans von Regisseuren und Filmen. Es gibt Insider-Informationen und exklusive Interviews."

const params = {
    loop: true,
    autoplay: {
        delay: 3000,
        disableOnInteraction: false
    },
    scrollbar: {
        el: '.swiper-scrollbar',
        hide: false
    }
}

export const query = graphql`
            query {
                editorial_001: file(relativePath:{ eq:"fanzine/editorial_001.png"}) {
                    ...foto
                },
                editorial_002: file(relativePath:{ eq:"fanzine/editorial_002.png"}) {
                    ...foto
                },
                editorial_003: file(relativePath:{ eq:"fanzine/editorial_003.jpg"}) {
                    ...foto
                },
                editorial_004: file(relativePath:{ eq:"fanzine/editorial_004.png"}) {
                    ...foto
                },
                editorial_005: file(relativePath:{ eq:"fanzine/editorial_005.png"}) {
                    ...foto
                },
            }
`

export default Fanzine
