import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import Layout from '../components/layout'

import { Navigation, WorkHeader, Footer, Container, WorkDescription } from '../components'

import LogoWestin from '../images/logo-collection/elbphilharmonie-logo.svg'
import LogoLouie from '../images/logo-collection/louie-logo.svg'
import LogoVolquardsen from '../images/logo-collection/volquardsen-logo.svg'
import LogoPizza from '../images/logo-collection/pizza-logo.svg'
import LogoAlumni from '../images/logo-collection/alumni-logo.svg'
import LogoVrtv from '../images/logo-collection/vrtv-logo.svg'

const LogoCollection = ({ data }) => (
    <Layout>
        <div className="logo-collection">
            <Navigation backButton />
            <WorkHeader
                headline="Logo Collection"
                label="Datum"
                subline="2014 – 2017"
            />
            <LogoCollectionItem logo={LogoWestin} bg={data.bgWestin.childImageSharp.fluid} link="https://www.the-saffron.de/de/the-bridge-bar" />
            <LogoCollectionItem logo={LogoLouie} bg={data.bgLouie.childImageSharp.fluid} dark />
            <LogoCollectionItem logo={LogoVolquardsen} bg={data.bgVolquardsen.childImageSharp.fluid} link="https://volquardsen.photo/de/" />
            <LogoCollectionItem logo={LogoPizza} bg={data.bgPizza.childImageSharp.fluid} />
            <LogoCollectionItem logo={LogoAlumni} bg={data.bgAlumni.childImageSharp.fluid} />
            <LogoCollectionItem logo={LogoVrtv} bg={data.bgVRTV.childImageSharp.fluid} />
            <Container width="large">
                <WorkDescription
                    copy="Eine repräsentative Auswahl meiner privaten, schulischen und beruflichen Logos."
                    team="brand:marke, Kunstschule Wandsbek"
                    tools="Illustrator CC, Pen&Paper"
                    client="Nivea, The Bridgebar, Volquardsen Fotografie"
                />
            </Container>
            <Footer small />
        </div>
    </Layout>
)

const LogoCollectionItem = (props) => (
    <div className={`logo-collection__item ${props.dark && "logo-collection__item--dark"}`}>
        <div className="logo-collection__wrapper">
            <Container>
                {props.link ?
                    <a href={props.link} className="logo-collection__link" target="_blanc">
                        <img className="logo-collection__img" src={props.logo} alt="logo" />
                    </a> :
                    <img className="logo-collection__img" src={props.logo} alt="logo" />
                }
            </Container>
        </div>
        <Img title="test" alt="test" fluid={props.bg} className="logo-collection__bg" />
    </div>
)

export const logoBG = graphql`
            fragment logoBG on File {
                childImageSharp {
                    fluid(maxWidth: 2560, maxHeight: 1440) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
`

export const query = graphql`
            query {
                bgWestin: file(relativePath:{ eq:"logo-collection/westin-bg.jpg"}) {
                    ...logoBG
                },
                bgLouie: file(relativePath:{ eq:"logo-collection/louie-bg.jpg"}) {
                    ...logoBG
                },
                bgVolquardsen: file(relativePath:{ eq:"logo-collection/volquardsen-bg.jpg"}) {
                    ...logoBG
                },
                bgPizza: file(relativePath:{ eq:"logo-collection/pizza-bg.jpg"}) {
                    ...logoBG
                },
                bgAlumni: file(relativePath:{ eq:"logo-collection/alumni-bg.jpg"}) {
                    ...logoBG
                },
                bgVRTV: file(relativePath:{ eq:"logo-collection/vrtv-bg.jpg"}) {
                    ...logoBG
                }
            }
`

export default LogoCollection
