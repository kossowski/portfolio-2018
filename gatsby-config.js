module.exports = {
  siteMetadata: {
    title: 'Dennis Kossowski | Portfolio',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Dennis Kossowski | Portfolio',
        short_name: 'Portfolio',
        start_url: '/',
        background_color: '#0F1A25',
        theme_color: '#0F1A25',
        display: 'minimal-ui',
        icon: 'src/images/icon.png', // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'images',
        path: `${__dirname}/src/images/lazyload`,
      }
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    'gatsby-plugin-offline',
    'gatsby-plugin-sass',
  ],
}
