# :zap: A NEW LOOK
This is the portfolio of Dennis Kossowski based on gatsbyJS.

Take a look on the [Live Website](https://denniskossowski.de/).

## Features
- Gatsby v2.0 based on reactJS
- SASS for styling
- Offline Support
- WebApp Manifest Support
- Lazy Load with the plugin [Gatsby-Image](https://www.gatsbyjs.org/packages/gatsby-image/?=gatsby%20i)
- Scroll-Animations from [AOS](https://michalsnik.github.io/aos/) 

## Getting Started
Check your development environment! You'll need [Node.js](https://nodejs.org/en/) and the [Gatsby CLI](https://www.gatsbyjs.org/docs/) installed. The official Gatsby website also lists two articles regarding this topic:
- [Gatsby on Windows](https://www.gatsbyjs.org/docs/gatsby-on-windows/)
- [Check your development environment](https://www.gatsbyjs.org/tutorial/part-zero/)



### Start the site

There are two slightly different ways to start the project:
1. Download or clone the repo `git clone git@gitlab.com:kossowski/portfolio-2018.git` in your local environment
2. Create a new project with `gatsby new project-name https://gitlab.com/kossowski/portfolio-2018.git`

Open the folder in your terminal and install the node packages, then you can run the project:
```
npm install
gatsby develop
```

### Adding new features/plugins

You can add other features by having a look at the offical [plugins page](https://www.gatsbyjs.org/docs/plugins/).

### Building the site

```
gatsby build
```

## Some planned features  

- [ ] SVG Header animation
- [ ] Behance-Projects
- [ ] Minor Text Changes